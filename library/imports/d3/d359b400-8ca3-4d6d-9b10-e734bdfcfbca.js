"use strict";
cc._RF.push(module, 'd359bQAjKNNbZsQ5zS9/PvK', 'PlayerScript');
// Scripts/PlayerScript.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
// can I add const??
var KEY_CODE_LEFT = 37;
var KEY_CODE_RIGHT = 39;
cc.Class({
  "extends": cc.Component,
  properties: {
    jumpHeight: 0,
    jumpDuratin: 0,
    accel: 0,
    maxMoveSpeed: 0,
    jumpAudio: {
      "default": null,
      type: cc.AudioClip
    }
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    this._leftDown = false;
    this._rightDown = false;
    this._xSpeed = 0;
  },
  onKeyDown: function onKeyDown(e) {
    // <
    if (e.keyCode == KEY_CODE_LEFT) {
      this._leftDown = true; // >
    } else if (e.keyCode == KEY_CODE_RIGHT) {
      this._rightDown = true;
    }
  },
  onKeyUp: function onKeyUp(e) {
    if (e.keyCode == KEY_CODE_LEFT) {
      this._leftDown = false; // >
    } else if (e.keyCode == KEY_CODE_RIGHT) {
      this._rightDown = false;
    }
  },
  start: function start() {
    // seems like move by is not work??
    // still not work??
    var actionUp = cc.moveBy(this.jumpDuratin, 0, this.jumpHeight).easing(cc.easeCubicActionOut());
    var actionDown = cc.moveBy(this.jumpDuratin, 0, -this.jumpHeight).easing(cc.easeCubicActionIn());
    var action = cc.sequence(actionUp, actionDown, cc.callFunc(this.playJumpSound, this));
    action = cc.repeatForever(action);
    this.node.runAction(action); // how to add the keyboard listen??

    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
  },
  playJumpSound: function playJumpSound() {
    cc.audioEngine.playEffect(this.jumpAudio, false);
  },
  // 单位为秒
  update: function update(dt) {
    if (this._leftDown) {
      this._xSpeed -= this.accel * dt;
    } else if (this._rightDown) {
      this._xSpeed += this.accel * dt;
    } else {
      this._xSpeed = 0;
    }

    if (Math.abs(this._xSpeed) > this.maxMoveSpeed) {
      this._xSpeed = this.maxMoveSpeed * this._xSpeed / Math.abs(this._xSpeed);
    }

    this.node.x += this._xSpeed * dt;
  }
});

cc._RF.pop();